<?php
    namespace Zimplify\Aws\Interfaces;
    use Zimplify\Aws\IamProfile;

    /**
     * this interface provide some basic common contracts for services connecting to AWS
     * @package Zimplify\AWS (code 22)
     * @type Interface (code 06)
     * @file IAwsServicesInterface (code 01)
     */
    interface IAwsServicesInterface {

        const ARGS_SETUP_IPN = "ipn";
        const ARGS_SETUP_REGION = "region";
        const ARGS_SETUP_VERSION = "version";     
        const ARGS_SETUP_KEY = "key";
        const ARGS_SETUP_SECRET = "secret";   
        const ARGS_SETUP_CREDENTIALS = "credentials";
        const CFG_AWS_VERSION = "vendor.aws.version";
        const ERR_NOT_SUPPORTED = 500220601001;
        const ERR_NOT_CONFIGURED = 500220601002;

    }