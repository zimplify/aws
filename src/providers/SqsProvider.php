<?php
    namespace Zimplify\Aws\Providers;
    use Zimplify\Core\{Application, Provider, Request};
    use Zimplify\Core\Services\{ArrayUtils};
    use Zimplify\Queues\Interfaces\IQueueServiceInterface;
    use Zimplify\Aws\Interfaces\IAwsServicesInterface;
    use Aws\Sqs\SqsClient;
    use Aws\Exception\AwsException;
    use \Exception;
    use \RuntimeException;

    /**
     * the Sqs Provider offer us the ability to send data over to SQS
     * @package Zimplify\Aws (code 22)
     * @type Provider (code 03)
     * @file SqsProvider (code 03)
     */           
    class SqsProvider extends Provider implements IAwsServicesInterface, IQueueServiceInterface {

        const ARGS_OPTIONS_METADATA = "metadata";
        const ARGS_RES_MESSAGES = "Messages";
        const ARGS_RES_MSG_BODY = "Body";
        const ARGS_SQS_QUEUE = "queue";                 // SQS Queue URL
        const ARGS_SQS_DATE =  "AttributeNames";
        const ARGS_SQS_READSIZE = "MaxNumberOfMessages";
        const ARGS_SQS_METADATA = "MessageAttributes";
        const ARGS_SQS_FILTER =  "MessageAttributeNames";
        const ARGS_SQS_QUEUE_PATH = "QueueUrl";
        const ARGS_SQS_PAUSE = "WaitTimeSeconds";
        const ARGS_SQS_MESSAGE = "Messages";
        const ARGS_SQS_MSG_BODY = "MessageBody";
        const ARGS_SQS_RECEIPT = "ReceiptHandle";        
        const ARGS_SQS_WAITTIME = "DelaySeconds";
        const CFG_SQS_SETUP = "vendor.aws.sqs";      
        const CFG_SQS_TYPE = "queue-type";
        const CFG_SQS_WAITTIME = "vendor.aws.sqs.wait";
        const DEF_SQS_FIFO = "fifo";
        const ERR_EMPTY_MESSAGE =  400220303001;
        const ERR_BAD_RESPONSE = 500220303002;
        const ERR_FAILED_READ = 500220303003;
        const ERR_UNKNOWN_RESPONSE = 500220303004;

        private $client; 

        /**
         * startup initializer for the service
         * @return void
         */
        protected function initialize() {
            parent::initialize();
            $setup = Application::env(self::CFG_SQS_SETUP);
            $this->debug("Config: ".json_encode($setup), __FUNCTION__);
            if (array_key_exists(self::ARGS_SETUP_KEY, $setup) && 
                array_key_exists(self::ARGS_SETUP_SECRET, $setup) && 
                array_key_exists(self::ARGS_SETUP_REGION, $setup)) {
                
                // region validation
                $regions = $this->driver("regions");
                if (in_array($setup[self::ARGS_SETUP_REGION], $regions)) {
                    $this->client = new SqsClient([
                        self::ARGS_SETUP_REGION => $setup[self::ARGS_SETUP_REGION],
                        self::ARGS_SETUP_VERSION => Application::env(self::CFG_AWS_VERSION),
                        self::ARGS_SETUP_CREDENTIALS => [
                            self::ARGS_SETUP_KEY => $setup[self::ARGS_SETUP_KEY],
                            self::ARGS_SETUP_SECRET => $setup[self::ARGS_SETUP_SECRET]
                        ]
                    ]);
                    // $this->client($setup[self::ARGS_SES_TOKEN], $setup[self::ARGS_SES_SECRET], $setup[self::ARGS_SETUP_REGION]);
                } else 
                    throw new RuntimeException("Region is not supported.", self::ERR_NOT_CONFIGURED);
            } else 
                throw new RuntimeException("Insufficient configuration for initialization.", self::ERR_NOT_CONFIGURED);
        }

        /**
         * check if the queue is ready for service
         * @return bool
         */
        public function isConnected() : bool {
            return true;
        }

        /**
         * check if all startup arguments are available
         * @return bool
         */
        protected function isRequired() : bool {
            $this->debug("ARGS: ".json_encode($this->get(self::ARGS_SQS_QUEUE)), __FUNCTION__);
            return !is_null($this->get(self::ARGS_SQS_QUEUE));
        }            

        /**
         * dispatching the data to the quuee
         * @param string $data the data we are piping through
         * @return bool
         */
        public function dispatch(Request $request) : string {
            
            $wait = Application::env(self::CFG_SQS_WAITTIME) ?? 1;
            $data = $request->data;
            $type = Application::env(self::CFG_SQS_TYPE);

            if (is_null($data)) 
                throw new RuntimeException("There is no data to send.", self::ERR_EMPTY_MESSAGE);

            // now extracting from request
            $message = [];
            $message[self::ARGS_SQS_QUEUE_PATH] = $this->get(self::ARGS_SQS_QUEUE);
            $message[self::ARGS_SQS_MSG_BODY] = $data;
            if ($type != self::DEF_SQS_FIFO)
                $message[self::ARGS_SQS_WAITTIME] = $wait;
            if (!is_null($request->metadata)) 
                $message[self::ARGS_SQS_METADATA] = $request->metadata;

            $this->debug("MESSAGE: ".json_encode($message), __FUNCTION__);

            // now sending
            $result = $this->client->sendMessage($message);

            // result extraction
            $this->debug("Response: ".$result->get("MessageId"), __FUNCTION__);
            $result = $result->get("MessageId");
            
            if (is_null($result))
                throw new RuntimeException("Unexpected result received.", self::ERR_BAD_RESPONSE);

            // return result
            return $result;
        }        

        /**
         * collect request from queue and for further processing
         * @param bool $clean whether the message must be cleaned after pickup
         * @param array $options (optional) addtional data we need during extraction
         * @return mixed
         */
        public function pickup(bool $clean = true, array $options = []) {
            $result = new Request();
            $metadata = array_key_exists(self::ARGS_OPTIONS_METADATA, $options) ? $options[self::ARGS_OPTIONS_METADATA] : null;

            // do the extract
            $data = $this->client->receiveMessage([
                self::ARGS_SQS_DATE => ['SentTimestamp'],
                self::ARGS_SQS_READSIZE => 1,
                self::ARGS_SQS_FILTER => $metadata ?? ['All'],
                self::ARGS_SQS_QUEUE_PATH => $this->get(self::ARGS_SQS_QUEUE),
                self::ARGS_SQS_PAUSE => 0
            ]);

            // now we have to make sure we extra

            $messages = $data->get(self::ARGS_RES_MESSAGES);
            if (!is_null($messages) && count($messages) > 0) {
                $message = $messages[0];
                $this->debug("MESSAGE: ".json_encode($message), __FUNCTION__);
                if (array_key_exists(self::ARGS_RES_MSG_BODY, $message)) {
                    $body = $message[self::ARGS_RES_MSG_BODY];
                    $decoded = json_decode($body, true);
                    $result->data = $decoded ?? $body;    

                    // make sure we remove the data
                    if ($clean === true) {
                        $this->debug("RECEIPT-HANDLER: ".$message[self::ARGS_SQS_RECEIPT], __FUNCTION__);
                        $this->client->deleteMessage([
                            self::ARGS_SQS_QUEUE_PATH => $this->get(self::ARGS_SQS_QUEUE),
                            self::ARGS_SQS_RECEIPT => $message[self::ARGS_SQS_RECEIPT]
                        ]);
                    }
                } else 
                    throw new RuntimeException("Unable to recognize message or empty body", self::ERR_UNKNOWN_RESPONSE);                
            } else 
                $result->data = null;

            // now outputing the result
            return $result;            
        }        
    }
