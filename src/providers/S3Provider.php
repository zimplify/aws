<?php
    namespace Zimplify\Aws\Providers;
    use Zimplify\Core\{Application, File, Provider};
    use Zimplify\Aws\IamProfile;
    use Zimplify\Aws\Interfaces\IAwsServicesInterface;
    use Aws\S3\S3Client;
    use Aws\S3\Exception\S3Exception;
    use \Exception;
    use \RuntimeException;

    /**
     * @package Zimplify\Aws (code 22)
     * @type Provider (code 03)
     * @file S3Provider (code 01)
     */    
    class S3Provider extends Provider implements IAwsServicesInterface {

        const ARGS_BUCKET = "bucket";
        const ARGS_S3_ACL = "ACL";
        const ARGS_S3_BUCKET = "Bucket";
        const ARGS_S3_CONTENT_TYPE = "ContentType";
        const ARGS_S3_FILE_KEY = "Key";
        const ARGS_S3_SOURCE = "SourceFile";
        const ARGS_S3_RES = "ObjectURL";
        const ARGS_SEED_SIZE = "seed";
        const CFG_S3_SETUP = "vendor.aws.s3";
        const DEF_ACL_TYPE = "public-read";
        const ERR_NO_FILE = 404220301001;
        const ERR_NO_CLIENT = 500220301002;

        // this is our AWS client
        private $client;

        /**
         * startup initializer for the service
         * @return void
         */
        protected function initialize() {
            parent::initialize();
            
            // determine our setup path
            $setup = Application::env(self::CFG_S3_SETUP);

            $this->debug("setup = ".json_encode($setup));

            // now check the fields
            if (array_key_exists(self::ARGS_SETUP_REGION, $setup) && array_key_exists(self::ARGS_SETUP_IPN, $setup)) {

                // region detection
                $regions = $this->driver("regions");
                if (!in_array($setup[self::ARGS_SETUP_REGION], $regions))
                    throw new RuntimeException("This region is not supported in SDK", self::ERR_NOT_SUPPORTED);

                if (!(array_key_exists(self::ARGS_SETUP_KEY, $setup) && array_key_exists(self::ARGS_SETUP_SECRET, $setup)))
                    throw new RuntimeException("Credentials data is not complete.", self::ERR_NOT_CONFIGURED);
                // creating our client
                $this->client = new S3Client([
                    self::ARGS_SETUP_REGION => $setup[self::ARGS_SETUP_REGION],
                    self::ARGS_SETUP_VERSION => Application::env(self::CFG_AWS_VERSION),
                    self::ARGS_SETUP_CREDENTIALS => [
                        self::ARGS_SETUP_KEY => $setup[self::ARGS_SETUP_KEY],
                        self::ARGS_SETUP_SECRET => $setup[self::ARGS_SETUP_SECRET]
                    ]
                ]);                
            } else
                throw new RuntimeException("Failed to initiate client", self::ERR_NO_CLIENT);
        }
        
        /**
         * check if all startup arguments are available
         * @return bool
         */
        protected function isRequired() : bool {
            return true;
        }

        /**
         * collect the file from S3 and store into buffer.
         * @param string $path the location of the file (this key must inclide file extension)
         * @return File
         */
        public function load(string $path) : File {

            // loading the data set from S3
            $data = $this->client->getObject([
                self::ARGS_S3_BUCKET => $this->get(self::ARGS_BUCKET),
                self::ARGS_S3_FILE_KEY => $path
            ]);

            // this is the structure when we return
            $result = new File();
            $result->output = true;
            $result->mime = $data->ContentType;
            $result->location = $key;
            $result->content = $data->body;
            $result->expiry = 0;
            $result->cached = false;
            $result->limitation = false;
            return $result;            
        }

        /**
         * generate a UUID based on the seed size required
         * @return string
         */
        protected function randomize() : string {
            $result = "";
            $seed = $this->driver()[self::ARGS_SEED_SIZE];
            for ($i = 0; $i < $seed ?? 3; $i++) 
                $result .= uniqid("", true);            
            return $result;
        }

        /**
         * storing the file into S3 and return thq FQDN of the file on S3.
         * @param File $source the file to store online
         * @param array $params the key params we need for execution
         * @return string
         */
        public function store(File $source, array $params) : string {

            // make sure our file is there
            if (is_readable($source->location) && array_key_exists(self::ARGS_BUCKET, $params)) {
                $filename = $this->randomize().".".$source->extension();
                $this->debug("file is store as $filename online");
                $this->debug("storing data onto bucket ".$params[self::ARGS_BUCKET]);

                // storing our file
                $response = $this->client->putObject([
                    self::ARGS_S3_ACL => $access ?? self::DEF_ACL_TYPE,
                    self::ARGS_S3_BUCKET => $params[self::ARGS_BUCKET],
                    self::ARGS_S3_CONTENT_TYPE => $source->mime,
                    self::ARGS_S3_FILE_KEY => $filename,
                    self::ARGS_S3_SOURCE => $source->location
                ]);
                
                // now return the result URL
                return $response[self::ARGS_S3_RES];
            } else
                throw new RuntimeException("Failed to locate the file to store", self::ERR_NO_FILE);
        }
    }