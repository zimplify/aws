<?php
    namespace Zimplify\Aws\Providers;
    use Zimplify\Core\{Application, File, Provider};
    use Zimplify\Core\Services\ClassUtils;
    use Zimplify\Aws\Interfaces\IAwsServicesInterface;
    use \Exception;
    use \RuntimeException;
    use \SimpleEmailServiceMessage;
    use \SimpleEmailService;

    /**
     * @package Zimplify\Aws (code 22)
     * @type Provider (code 03)
     * @file SesProvider (code 02)
     */       
    class SesProvider extends Provider implements IAwsServiceInterface {

        const ARGS_SES_TOKEN = "token";
        const ARGS_SES_SECRET = "secret";
        const CFG_SES_SETUP = "vendor.aws.ses";
        const ERR_NOT_CONFIGURED = 500220302001;
        const ERR_FAILED_SEND = 500220302002;
        const ERR_NO_RECEIVERS = 400220302003;

        private $client;

        /**
         * startup initializer for the service
         * @return void
         */
        protected function initialize() {
            parent::initialize();
            $setup = Application::env(self::CFG_SES_SETUP);
            if (array_key_exists(self::ARGS_SES_SECRET, $setup) && 
                array_key_exists(self::ARGS_SES_TOKEN, $setup) && 
                array_key_exists(self::ARGS_SETUP_REGION, $setup)) {
                
                // region validation
                $regions = $this->driver("regions");
                if (array_key_exists($setup[self::ARGS_SETUP_REGION], $regions))
                    $this->client($setup[self::ARGS_SES_TOKEN], $setup[self::ARGS_SES_SECRET], $setup[self::ARGS_SETUP_REGION]);
                else 
                    throw new RuntimeException("Region is not supported.", self::ERR_NOT_CONFIGURED);
            } else 
                throw new RuntimeException("Insufficient configuration for initialization.", self::ERR_NOT_CONFIGURED);
        }

        /**
         * check if all startup arguments are available
         * @return bool
         */
        protected function isRequired() : bool {
            return true;
        }        

        /**
         * sending the message
         * @param array $sender the sender info (0 = email, 1 = name)
         * @param array $receiver the sender info (0 = email, 1 = name)
         * @param string $body the HTML based data of the message body
         * @param string $title the title of the message
         * @param array $attachments the list of file code to extract from as attachments
         * @return bool
         */
        public function send(array $sender, array $receiver, string $body, string $title = "", array $attachments = []) : bool {
            $message = new SimpleEmailServiceMessage();
            $message->setFrom($sender[0]);
            $message = $this->setReceipients($message, $receiver);
            $message->setSubject($title);
            $message->setMessageFromString($body);
            $message = $this->setAttachments($message, $attachments);
            $result = $this->client->sendEmail($message);
            if ($result) {
                return true;
            } else 
                throw new RuntimeException("Unable to send message.", self::ERR_FAILED_SEND);
        }        

        /**
         * adding attachments onto the message
         * @param SimpleEmailServiceMessage $message the message to send
         * @param array $attachments the files to add
         * @return SimpleEmailServiceMessage
         */
        protected function setAttachments(SimpleEmailServiceMessage $message, array $attachments) : SimpleEmailServiceMessage {
            foreach ($attachments as $attachment) {
                if (ClassUtils::is($attachment, File::DEF_CLS_NAME)) {         
                    $path = $attachment->location;
                    $name = explode("/", $path);       
                    $message->addAttachmentFromFile(end($name), file_get_contents($path), $attachment->mime);
                }
            } 
            return $message;
        }

        /**
         * adding all receipients onto the message
         * @param SimpleEmailServiceMessage $message the message to send
         * @param array $receivers the reciver for the message
         * @return SimpleEmailServiceMessage
         */
        protected function setReceipients(SimpleEmailServiceMessage $message, array $receivers) : SimpleEmailServiceMessage {
            if (count($receivers) > 0) {
                foreach ($receivers as $receiver) 
                    $message->addTo($receiver[0]);                
                return $message;
            } else 
                throw new RuntimeException("There are no receipients.", self::ERR_NO_RECEIVERS);
        }
    }