<?php
    namespace Zimplify\Aws;
    use Aws\Iam\IamClient;
    use Zimplify\Core\Application;

    /**
     * the IAM profile is our security gate for all services on AWS
     * @package Zimplify\AWS (code 22)
     * @type Instance (code 01)
     * @file IamProfile (code 01)
     */
    class IamProfile {

        const ARGS_IAM_ARN = "Arn";
        const ARGS_IAM_INSTANCE = "InstanceId";
        const ARGS_IAM_INST_PROFILE = "IamInstanceProfile";
        const ARGS_IAM_PROFILE_INSTANCE = "InstanceProfileId";
        const ARGS_IAM_PROFILE_REF = "InstanceProfileName";   
        const ARGS_IAM_PROFILE_NAME = "Name";
        const CFG_AWS_VERSION = "vendor.aws.version";
        const ERR_NO_PROFILE = 500220101001;

        private $arn;
        private $instance;

        /**
         * creating the instance of the profile
         * @param string $region the AWS region we are using resources
         * @param string $profile the instance profile we are using - assuming already defined on AWS IAM console
         * @return void
         */
        function __construct(string $region, string $profile) {
            $client = new IamClient(["region" => $region, "version" => Application::env(self::CFG_AWS_VERSION)]);
            $response = $client->createInstanceProfile([self::ARGS_IAM_PROFILE_REF => "InstanceProfileName"]);
            if (array_key_exists(self::ARGS_IAM_ARN, $response) && array_key_exists(self::ARGS_IAM_PROFILE_INSTANCE, $response)) {
                $this->arn = $response[self::ARGS_IAM_ARN];
                $this->instance = $response[self::ARGS_IAM_PROFILE_INSTANCE];                
            } else 
                throw new RuntimeException("Unable to generate IAM Profile.", self::ERR_NO_PROFILE);
        }

        /**
         * exporting the profile into data array so it can be assumed in SDK
         * @return array
         */
        public function export() : array {
            return [
                self::ARGS_IAM_INST_PROFILE => [
                    self::ARGS_IAM_ARN => $arn,
                    self::ARGS_IAM_PROFILE_NAME => $profile
                ],
                self::ARGS_IAM_INSTANCE => $instance
            ];            
        }
    }