# AWS Integration Package for Zimplify Apps
This package is designed to help with the integration work for AWS. This package at current stage does not complete the deployment of AWS elements,
which will be supported in later release

## Installation
Simply composer the pacakge
````
composer require zimplify\aws
````

## Configuration
The configuration parameters are listed under `package.json` file and will automatically populate onto your environment `$ZIMPLIFY_APP/lib/app/app.operation.config`. 
````
"vendor": {
    "aws": {
        "version": "latest",                        // the version of AWS SDK to apply
        "ecs": {                                    // configuration for ECS
        },
        "s3": {                                     // configuration for S3
            "key": "<your IAM key for S3",          // the access key from IAM for S3 user
            "secret": "<your IAM secret for key>",  // the secret key for service
            "region": "<product region on AWS>"     // product region
        },
        "sns": {                                    // configuration for SNS            
        }
        "sqs": {                                    // configuration for SQS
            "wait": 1,                              // the SQS wait time for message in queue expose to clients
            "key": "<your IAM key for SQS",         // the access key from IAM for SQS user
            "secret": "<your IAM secret for key>",  // the secret key for service
            "region": "<product region on AWS>",    // product region
            "queue-type": ""                        // the nature of the SQS queue -> if is FIFO wait time will not be required
        }
    }
}
````
## Usage
To use each of the service you can access via the Application construct.
````
use Zimplify\Core\{Application, Request};                   // Works after PHP 5.6
...
$request = new Request();                                   // define a new request
$request->data = ["test" => $value];                        // assign some value
$adapter = Application::request("aws::sqs", []);            // loading our adapter out
$receipt = $adapter->dispatch($request);                    // sending out the request
var_dump($receipt);                                         // peeking what is inside
...
````
Typically you will be using these adapters with wrappers like EmailProvider, QueueProvider, etc so you shouldn't have to deal with each low level declaration directly.
